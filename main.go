package main

import (
"net/http"

"github.com/labstack/echo/v4"
)

func main() {
e := echo.New()
e.GET("/", func(c echo.Context) error {
return c.String(http.StatusOK, "Funciona la api en tu sistema!")
})
e.Logger.Fatal(e.Start(":8080"))
}
